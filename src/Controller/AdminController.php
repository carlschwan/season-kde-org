<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\MentorApplication;

class AdminController extends EasyAdminController
{
    public function acceptAction()
    {
        // change the properties of the given entity and save the changes
        $id = $this->request->query->get('id');
        $mentorApplication = $this->em->getRepository(MentorApplication::class)->find($id);
        $user = $mentorApplication->getUser();
        $user->setMentor(true);
        $this->em->remove($mentorApplication);
        $this->em->flush();

        return new Response($mentorApplication->getUser());

        return $this->redirectToRoute('easyadmin', array(
            'action' => 'list',
            'entity' => $this->request->query->get('entity'),
        ));
    }
}
