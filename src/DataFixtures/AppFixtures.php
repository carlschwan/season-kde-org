<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\Season;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $season = new Season();
        $season->setTitle("Season of KDE 2025");
        $season->setDescription("A wonderfull season for contributing of KDE");
        $season->setStartTime(new \DateTime());
        $season->setEndTime(new \DateTime());
        $season->setActive(true);
        $season->setDeadlineStudent(new \DateTime());
        $season->setDeadlineMentor(new \DateTime());

        $manager->persist($season);

        $manager->flush();
    }
}
